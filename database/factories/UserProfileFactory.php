<?php

use App\UserProfile;
use Faker\Generator as Faker;

$factory->define(UserProfile::class, function (Faker $faker) {
    return [
        'bio' => $faker->paragraph,
    ];
});
