<?php

use App\Profession;
use App\User;
use App\UserProfile;
use Illuminate\Database\Seeder;
use App\Skill;
use App\Team;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $professions = Profession::all();
        $skills = Skill::all();
        $teams = Team::all();

        $user = factory(User::class)->create([
            'first_name'    => 'Jonathan',
            'last_name'     => 'Zarate',
            'email'         => 'zaratedev@gmail.com',
            'password'      => bcrypt('secret'),
            'role'          => 'admin',
            'team_id'       => $teams->firstWhere('name', 'dinkbit'),
            'created_at'    => now()->addDay(),
            'active'        => true,
        ]);

        $user->profile()->create([
            'bio'           => 'Developer',
            'profession_id' => $professions->firstWhere('title', 'Desarrollador back-end')->id,

        ]);

        foreach (range(0, 999) as $i) {
            $user = factory(User::class)->create([
                'team_id' => rand(0, 2) ? null : $teams->random()->id,
                'active' => rand(0, 3) ? true : false,
            ]);

            $user->skills()->attach($skills->random(rand(0, 6)));

            factory(UserProfile::class)->create([
                'user_id' => $user->id,
                'profession_id' => rand(0, 2) ? $professions->random()->id : null,
            ]);
        }
        
    }
}
