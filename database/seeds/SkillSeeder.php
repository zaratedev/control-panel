<?php

use Illuminate\Database\Seeder;
use App\Skill;

class SkillSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Skill::class)->create(['name' => 'Laravel']);
        factory(Skill::class)->create(['name' => 'PHP']);
        factory(Skill::class)->create(['name' => 'Vue']);
        factory(Skill::class)->create(['name' => 'TDD']);
        factory(Skill::class)->create(['name' => 'Js']);
        factory(Skill::class)->create(['name' => 'Webpack']);
    }
}
