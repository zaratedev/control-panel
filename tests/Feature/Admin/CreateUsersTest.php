<?php

namespace Tests\Feature\Admin;

use App\Profession;
use App\Skill;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CreateUsersTest extends TestCase
{
    use RefreshDatabase;
    /** @test */
    public function it_loads_the_new_users_page()
    {
        $this->withoutExceptionHandling();
        $profession = factory(Profession::class)->create();

        $oneSkill = factory(Skill::class)->create();
        $twoSkill = factory(Skill::class)->create();

        $this->get(route('users.create'))
                ->assertViewIs('users.create')
                ->assertStatus(200)
                ->assertSee('Crear usuario')
                ->assertViewHas('professions', function ($professions) use ($profession) {
                    return $professions->contains($profession);
                })
                ->assertViewHas('skills', function ($skills) use ($oneSkill, $twoSkill) {
                    return $skills->contains($oneSkill) && $skills->contains($twoSkill);
                });
    }

    /** @test */
    public function it_creates_a_new_user()
    {
        $this->withoutExceptionHandling();

        $skillA = factory(Skill::class)->create();
        $skillB = factory(Skill::class)->create();
        $skillC = factory(Skill::class)->create();

        $this->post('/usuarios/', $this->getValidData([
                'skills' => [$skillA->id, $skillB->id],
            ]))->assertRedirect('usuarios');

        $this->assertCredentials([
                'first_name'    => 'Jonathan',
                'last_name'     => 'Zarate',
                'email'    => 'jon@mail.com',
                'password' => '123456',
                'role'     => 'user',
            ]);

        $user = User::findByEmail('jon@mail.com');

        $this->assertDatabaseHas('user_profiles', [
                'bio'           => 'Backend developer',
                'twitter'       => 'https://twitter.com/zaratedev',
                'user_id'       => $user->id,
                'profession_id' => $this->profession->id,
            ]);

        $this->assertDatabaseHas('user_skill', [
                'user_id'  => $user->id,
                'skill_id' => $skillA->id,
            ]);

        $this->assertDatabaseHas('user_skill', [
                'user_id'  => $user->id,
                'skill_id' => $skillB->id,
            ]);

        $this->assertDatabaseMissing('user_skill', [
                'user_id'  => $user->id,
                'skill_id' => $skillC->id,
            ]);
    }

    /** @test */
    public function the_role_field_is_optional()
    {
        $this->withoutExceptionHandling();

        $this->post('/usuarios/', $this->getValidData([
                'role' => null,
            ]))->assertRedirect('usuarios');

        $this->assertDatabaseHas('users', [
                'email' => 'jon@mail.com',
                'role'  => 'user',
            ]);
    }

    /** @test */
    public function the_role_must_is_optional()
    {
        $this->withExceptionHandling();

        $this->post('/usuarios/', $this->getValidData([
                'role' => 'invalid-role',
            ]))->assertSessionHasErrors('role');

        $this->assertDatabaseMissing('users', [
                'email' => 'jon@mail.com',
            ]);
    }

    /** @test */
    public function the_first_name_is_required()
    {
        $this->withExceptionHandling();

        $this->from('usuarios/nuevo')
                ->post('/usuarios/', [
                    'first_name'     => '',
                    'email'    => 'duilio@styde.net',
                    'password' => '123456',
                ])
                ->assertRedirect('usuarios/nuevo')
                ->assertSessionHasErrors(['first_name']);

        $this->assertEquals(0, User::count());
    }

    /** @test */
    public function the_last_name_is_required()
    {
        $this->withExceptionHandling();

        $this->from('usuarios/nuevo')
                ->post('/usuarios/', [
                    'last_name'     => '',
                    'email'    => 'duilio@styde.net',
                    'password' => '123456',
                ])
                ->assertRedirect('usuarios/nuevo')
                ->assertSessionHasErrors(['last_name']);

        $this->assertEquals(0, User::count());
    }

    /** @test */
    public function the_email_is_required()
    {
        $this->withExceptionHandling();

        $this->from('usuarios/nuevo')
                ->post('/usuarios/', [
                    'first_name'     => 'Duilio',
                    'email'    => '',
                    'password' => '123456',
                ])
                ->assertRedirect('usuarios/nuevo')
                ->assertSessionHasErrors(['email']);

        $this->assertEquals(0, User::count());
    }

    /** @test */
    public function the_email_must_be_valid()
    {
        $this->withExceptionHandling();

        $this->from('usuarios/nuevo')
                ->post('/usuarios/', [
                    'first_name'     => 'Duilio',
                    'email'    => 'correo-no-valido',
                    'password' => '123456',
                ])
                ->assertRedirect('usuarios/nuevo')
                ->assertSessionHasErrors(['email']);

        $this->assertEquals(0, User::count());
    }

    /** @test */
    public function the_email_must_be_unique()
    {
        $this->withExceptionHandling();

        factory(User::class)->create([
                'email' => 'duilio@styde.net',
            ]);

        $this->from('usuarios/nuevo')
                ->post('/usuarios/', [
                    'first_name'     => 'Duilio',
                    'email'    => 'duilio@styde.net',
                    'password' => '123456',
                ])
                ->assertRedirect('usuarios/nuevo')
                ->assertSessionHasErrors(['email']);

        $this->assertEquals(1, User::count());
    }

    /** @test */
    public function the_password_is_required()
    {
        $this->withExceptionHandling();

        $this->from('usuarios/nuevo')
                ->post('/usuarios/', [
                    'first_name'     => 'Duilio',
                    'email'    => 'duilio@styde.net',
                    'password' => '',
                ])
                ->assertRedirect('usuarios/nuevo')
                ->assertSessionHasErrors(['password']);

        $this->assertEquals(0, User::count());
    }

    /** @test */
    public function the_profession_must_be_valid()
    {
        $this->withExceptionHandling();
        
        $this->withExceptionHandling();
        $this->from('usuarios/nuevo')
                ->post('/usuarios/', $this->getValidData([
                    'profession_id' => '999',
                ]))
                ->assertRedirect('usuarios/nuevo')
                ->assertSessionHasErrors(['profession_id']);

        $this->assertEquals(0, User::count());
    }

    /** @test */
    public function the_another_profession_field_is_optional()
    {
        $this->withoutExceptionHandling();

        $this->post('/usuarios/', $this->getValidData([
                'another_profession' => null,
            ]))->assertRedirect('usuarios');

        $this->assertCredentials([
                'first_name'     => 'Jonathan',
                'email'    => 'jon@mail.com',
                'password' => '123456',
            ]);

        $this->assertDatabaseHas('user_profiles', [
                'bio'                  => 'Backend developer',
                'another_profession'   => null,
                'user_id'              => User::findByEmail('jon@mail.com')->id,
            ]);
    }

    protected function getValidData(array $custom = [])
    {
        $this->profession = factory(Profession::class)->create();

        return array_filter(array_merge([
            'first_name'    => 'Jonathan',
            'last_name'     => 'Zarate',
            'email'         => 'jon@mail.com',
            'password'      => '123456',
            'profession_id' => $this->profession->id,
            'bio'           => 'Backend developer',
            'twitter'       => 'https://twitter.com/zaratedev',
            'role'          => 'user',
            'skills'        => [],
        ], $custom));
    }
}
