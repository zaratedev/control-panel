<?php

namespace Tests\Feature\Admin;

use App\User;
use Tests\TestCase;
use App\Profession;
use App\UserProfile;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserProfileTest extends TestCase
{
    use RefreshDatabase;

    protected $defaultData = [
        'first_name' => 'john',
        'last_name' => 'zarate',
        'email'    => 'john@example.com',
        'password' => '123456',
        'bio'      => 'Developer web',
        'twitter'  => 'https://twitter.com/john',
    ];

    /** @test */
    public function a_user_can_edit_its_profile()
    {
        $user = factory(User::class)->create();
        $user->profile()->save(factory(UserProfile::class)->make());

        $newProfession = factory(Profession::class)->create();

        $response = $this->get('/editar-perfil/');

        $response->assertStatus(200);

        $response = $this->put('/editar-perfil/', [
            'first_name'         => 'Jonathan',
            'last_name'         => 'Zarate',
            'email'              => 'duilio@styde.net',
            'bio'                => 'Programador de Laravel y Vue.js',
            'twitter'            => 'https://twitter.com/sileence',
            'profession_id'      => $newProfession->id,
        ]);

        $response->assertRedirect();

        $this->assertDatabaseHas('users', [
            'first_name'  => 'Jonathan',
            'email' => 'duilio@styde.net',
        ]);

        $this->assertDatabaseHas('user_profiles', [
            'bio'           => 'Programador de Laravel y Vue.js',
            'twitter'       => 'https://twitter.com/sileence',
            'profession_id' => $newProfession->id,
        ]);
    }

    /** @test */
    public function the_user_cannot_change_its_role()
    {
        $user = factory(User::class)->create([
            'role' => 'user',
        ]);

        $response = $this->put('/editar-perfil/', $this->getValidData([
            'role' => 'admin',
        ]));

        $response->assertRedirect();

        $this->assertDatabaseHas('users', [
            'id'   => $user->id,
            'role' => 'user',
        ]);
    }

    /** @test */
    public function the_user_cannot_change_its_password()
    {
        factory(User::class)->create([
            'password' => bcrypt('old123'),
        ]);

        $response = $this->put('/editar-perfil/', $this->getValidData([
            'email'    => 'duilio@styde.net',
            'password' => 'new456',
        ]));

        $response->assertRedirect();

        $this->assertCredentials([
            'email'    => 'duilio@styde.net',
            'password' => 'old123',
        ]);
    }

    protected function getValidData(array $custom = [])
    {
        $profession = factory(Profession::class)->create();

        return array_filter(array_merge([
            'first_name'    => 'Jonathan',
            'last_name'    => 'Zarate',
            'email'         => 'jon@mail.com',
            'password'      => '123456',
            'profession_id' => $profession->id,
            'bio'           => 'Backend developer',
            'twitter'       => 'https://twitter.com/zaratedev',
            'role'          => 'user',
        ], $custom));
    }
}
