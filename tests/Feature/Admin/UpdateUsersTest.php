<?php

namespace Tests\Feature\Admin;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\User;
use App\Profession;
use App\UserProfile;
use App\Skill;

class UpdateUsersTest extends TestCase
{
    use RefreshDatabase;
    /** @test */
    public function it_loads_the_edit_user_page()
    {
        $user = factory(User::class)->create();

        $this->get("/usuarios/{$user->id}/editar") // usuarios/5/editar
            ->assertStatus(200)
            ->assertViewIs('users.edit')
            ->assertSee('Editar usuario')
            ->assertViewHas('user', function ($viewUser) use ($user) {
                return $viewUser->id === $user->id;
            });
    }

    /** @test */
    public function it_updates_a_user()
    {
        $user = factory(User::class)->create();

        $oldProfession = factory(Profession::class)->create();

        $user->profile()->save(factory(UserProfile::class)->make([
            'profession_id' => $oldProfession->id,
        ]));

        $oldSkillOne = factory(Skill::class)->create();
        $oldSkillTwo = factory(Skill::class)->create();

        $user->skills()->attach([$oldSkillOne->id, $oldSkillTwo->id]);

        $newProfession = factory(Profession::class)->create();
        $skillOne = factory(Skill::class)->create();
        $skillTwo = factory(Skill::class)->create();

        $this->put("/usuarios/{$user->id}", [
            'first_name'     => 'Jonathan',
            'last_name'     => 'Zarate',
            'email'    => 'jon@mail.com',
            'password' => '123456',
            'bio'           => 'Backend developer',
            'twitter'       => 'https://twitter.com/zaratedev',
            'role'          => 'admin',
            'profession_id' => $newProfession->id,
            'skills'        => [$skillOne->id, $skillTwo->id],
        ])->assertRedirect("/usuarios/{$user->id}");

        $this->assertCredentials([
            'first_name'     => 'Jonathan',
            'last_name'     => 'Zarate',
            'email'    => 'jon@mail.com',
            'password' => '123456',
            'role' => 'admin',
        ]);

        $this->assertDatabaseHas('user_profiles', [
            'user_id' => $user->id,
            'bio' => 'Backend developer',
            'twitter' => 'https://twitter.com/zaratedev',
            'profession_id' => $newProfession->id,
        ]);
    }

    /** @test */
    public function the_name_is_required_when_updating_the_user()
    {
        $this->withExceptionHandling();

        $user = factory(User::class)->create();

        $this->from("usuarios/{$user->id}/editar")
            ->put("usuarios/{$user->id}", [
                'first_name'     => '',
                'email'    => 'duilio@styde.net',
                'password' => '123456',
            ])
            ->assertRedirect("usuarios/{$user->id}/editar")
            ->assertSessionHasErrors(['first_name']);

        $this->assertDatabaseMissing('users', ['email' => 'duilio@styde.net']);
    }

    /** @test */
    public function the_email_must_be_valid_when_updating_the_user()
    {
        $this->withExceptionHandling();

        $user = factory(User::class)->create();

        $this->from("usuarios/{$user->id}/editar")
            ->put("usuarios/{$user->id}", [
                'first_name'     => 'Duilio Palacios',
                'email'    => 'correo-no-valido',
                'password' => '123456',
            ])
            ->assertRedirect("usuarios/{$user->id}/editar")
            ->assertSessionHasErrors(['email']);

        $this->assertDatabaseMissing('users', ['first_name' => 'Duilio Palacios']);
    }

    /** @test */
    public function the_email_must_be_unique_when_updating_the_user()
    {
        $this->withExceptionHandling();
        
        factory(User::class)->create([
            'email' => 'existing-email@example.com',
        ]);

        $user = factory(User::class)->create([
            'email' => 'duilio@styde.net',
        ]);

        $this->from("usuarios/{$user->id}/editar")
            ->put("usuarios/{$user->id}", [
                'first_name'     => 'Duilio',
                'email'    => 'existing-email@example.com',
                'password' => '123456',
                'bio'           => 'Backend developer',
                'twitter'       => 'https://twitter.com/zaratedev',
                'role'          => 'admin',
                'profession_id' => '',
                'skills'        => [],
            ])
            ->assertRedirect("usuarios/{$user->id}/editar")
            ->assertSessionHasErrors(['email']);
    }

    /** @test */
    public function the_users_email_can_stay_the_same_when_updating_the_user()
    {
        $user = factory(User::class)->create([
            'email' => 'duilio@styde.net',
        ]);

        $this->from("usuarios/{$user->id}/editar")
            ->put("usuarios/{$user->id}", [
                'first_name'     => 'Jonathan',
                'last_name'     => 'Zarate',
                'email'    => 'duilio@styde.net',
                'password' => '12345678',
                'bio'           => 'Backend developer',
                'twitter'       => 'https://twitter.com/zaratedev',
                'role'          => 'admin',
                'profession_id' => '',
                'another_profession' => '',
                'skills'        => [],
            ])
            ->assertRedirect("usuarios/{$user->id}"); // (users.show)

        $this->assertDatabaseHas('users', [
            'first_name'  => 'Jonathan',
            'email' => 'duilio@styde.net',
        ]);
    }

    /** @test */
    public function the_password_is_optional_when_updating_the_user()
    {
        $oldPassword = 'CLAVE_ANTERIOR';

        $user = factory(User::class)->create([
            'password' => bcrypt($oldPassword),
        ]);

        $this->from("usuarios/{$user->id}/editar")
            ->put("usuarios/{$user->id}", [
                'first_name'     => 'Duilio',
                'last_name'     => 'Palacios',
                'email'    => 'duilio@styde.net',
                'password' => '',
                'bio'           => 'Backend developer',
                'twitter'       => 'https://twitter.com/zaratedev',
                'role'          => 'admin',
                'profession_id' => '',
                'skills'        => [],
            ])
            ->assertRedirect("usuarios/{$user->id}"); // (users.show)

        $this->assertCredentials([
            'first_name'     => 'Duilio',
            'email'    => 'duilio@styde.net',
            'password' => $oldPassword, // VERY IMPORTANT!
        ]);
    }

    protected function getValidData(array $custom = [])
    {
        $this->profession = factory(Profession::class)->create();

        return array_filter(array_merge([
            'first_name'    => 'Jonathan',
            'last_name'     => 'Zarate',
            'email'         => 'jon@mail.com',
            'password'      => '123456',
            'profession_id' => $this->profession->id,
            'bio'           => 'Backend developer',
            'twitter'       => 'https://twitter.com/zaratedev',
            'role'          => 'user',
        ], $custom));
    }
}
