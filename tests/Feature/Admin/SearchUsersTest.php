<?php

namespace Tests\Feature\Admin;

use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Team;

class SearchUsersTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function search_users_by_name()
    {
        $joel = factory(User::class)->create([
            'first_name' => 'Joel',
        ]);

        $ellie = factory(User::class)->create([
            'first_name' => 'Ellie',
        ]);

        $this->get('/usuarios?search=Joel')
            ->assertSee('Listado de usuarios')
            ->assertViewHas('users', function ($users) use ($joel, $ellie) {
                return $users->contains($joel) && !$users->contains($ellie);
            });
    }

    /** @test */
    public function show_results_with_a_partial_search_by_name()
    {
        $joel = factory(User::class)->create([
            'first_name' => 'Joel',
        ]);

        $ellie = factory(User::class)->create([
            'first_name' => 'Ellie',
        ]);

        $this->get('/usuarios?search=Jo')
            ->assertSee('Listado de usuarios')
            ->assertViewHas('users', function ($users) use ($joel, $ellie) {
                return $users->contains($joel) && !$users->contains($ellie);
            });
    }

    /** @test */
    public function search_users_full_name()
    {
        $joel = factory(User::class)->create([
            'first_name' => 'Joel',
            'last_name' => 'Miller',
        ]);

        $ellie = factory(User::class)->create([
            'first_name' => 'Ellie',
            'last_name' => 'Williams',
        ]);

        $this->get('/usuarios?search=Joel')
            ->assertSee('Listado de usuarios')
            ->assertViewHas('users', function ($users) use ($joel, $ellie) {
                return $users->contains($joel) && !$users->contains($ellie);
            });
    }

    /** @test */
    public function partial_search_by_full_name()
    {
        $joel = factory(User::class)->create([
            'first_name' => 'Joel',
            'last_name' => 'Miller',
        ]);

        $ellie = factory(User::class)->create([
            'first_name' => 'Ellie',
            'last_name' => 'Williams',
        ]);

        $this->get('/usuarios?search=Joel M')
            ->assertSee('Listado de usuarios')
            ->assertViewHas('users', function ($users) use ($joel, $ellie) {
                return $users->contains($joel) && !$users->contains($ellie);
            });
    }

    /** @test */
    public function search_users_by_email()
    {
        $joel = factory(User::class)->create([
            'email' => 'joel@example.com',
        ]);

        $ellie = factory(User::class)->create([
            'email' => 'ellie@example.com',
        ]);

        $this->get('/usuarios?search=joel@example.com')
            ->assertSee('Listado de usuarios')
            ->assertViewHas('users', function ($users) use ($joel, $ellie) {
                return $users->contains($joel) && !$users->contains($ellie);
            });
    }

    /** @test */
    public function show_results_with_a_partial_search_by_email()
    {
        $joel = factory(User::class)->create([
            'email' => 'joel@example.com',
        ]);

        $ellie = factory(User::class)->create([
            'email' => 'ellie@example.net',
        ]);

        $this->get('/usuarios?search=@example.com')
            ->assertSee('Listado de usuarios')
            ->assertViewHas('users', function ($users) use ($joel, $ellie) {
                return $users->contains($joel) && !$users->contains($ellie);
            });
    }

    /** @test */
    public function search_users_by_team_name()
    {
        $joel = factory(User::class)->create([
            'first_name' => 'Joel',
            'team_id' => factory(Team::class)->create(['name' => 'dinkbit'])->id,
        ]);

        $ellie = factory(User::class)->create([
            'first_name' => 'Ellie',
            'team_id' => null,
        ]);

        $erick = factory(User::class)->create([
            'first_name' => 'Erick',
            'team_id' => factory(Team::class)->create(['name' => 'Fire'])->id,
        ]);

        $response = $this->get('/usuarios?search=Fire')
            ->assertSee('Listado de usuarios');
        // ->assertViewHas('users', function($users) use ($erick, $joel, $ellie) {
        //     return $users->contains($erick)
        //     && !$users->contains($joel)
        //     && !$users->contains($ellie);
        // });
        
        $response->assertViewCollection('users')
            ->contains($erick)
            ->notContains($joel)
            ->notContains($ellie);
    }
}
