<?php

namespace Tests\Feature\Admin;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Profession;

class ListProfessionsTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_shows_the_users_list()
    {
        factory(Profession::class)->create(['title' => 'Developer']);
        factory(Profession::class)->create(['title' => 'Admin']);
        factory(Profession::class)->create(['title' => 'Manager']);

        $this->get('/profesiones')
            ->assertStatus(200)
            ->assertSeeInOrder([
                'Admin',
                'Developer',
                'Manager',
            ]);
    }
}
