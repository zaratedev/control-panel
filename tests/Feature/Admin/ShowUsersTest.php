<?php

namespace Tests\Feature\Admin;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\User;

class ShowUsersTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_displays_the_users_details()
    {
        $user = factory(User::class)->create([
            'first_name' => 'Jonathan',
            'last_name' => 'Zarate',
        ]);

        $this->get("/usuarios/{$user->id}") // usuarios/5
            ->assertStatus(200)
            ->assertSee('Jonathan Zarate');
    }

    /** @test */
    public function it_displays_a_404_error_if_the_user_is_not_found()
    {
        $this->withExceptionHandling();
        
        $this->get('/usuarios/999')
            ->assertStatus(404)
            ->assertSee('Página no encontrada');
    }
}
