<?php

namespace Tests\Browser\Admin;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\Profession;
use App\Skill;

class CreateUserTest extends DuskTestCase
{
    use DatabaseMigrations;

    
    /** @test */
    public function a_user_can_be_created()
    {
        $profession = factory(Profession::class)->create();
        $skillA = factory(Skill::class)->create();
        $skillB = factory(Skill::class)->create();
    
        $this->browse(function (Browser $browser, $browser2) use ($profession, $skillA, $skillB) {
            $browser->visit('usuarios/nuevo')
                    ->type('name', 'Jonathan')
                    ->type('email', 'john@mail.com')
                    ->type('password', 'secret')
                    ->type('bio', 'Developer backend')
                    ->select('profession_id', $profession->id)
                    ->type('twitter', 'https://twitter.com/zaratedev')
                    ->check("skills[{$skillA->id}]")
                    ->check("skills[{$skillB->id}]")
                    ->radio('role', 'user')
                    ->press('Crear usuario')
                    ->assertPathIs('/usuarios')
                    ->assertSee('Jonathan');
        });
    }
}
