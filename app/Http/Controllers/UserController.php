<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateUserRequest;
use App\Skill;
use App\Profession;
use App\User;
use App\Http\Requests\UpdateUserRequest;

class UserController extends Controller
{
    public function index()
    {
        $users = User::query()
                    ->with('team', 'skills', 'profile.profession')
                    ->byState(request('state'))
                    ->search(request('search'))
                    ->orderByDesc('created_at')
                    ->paginate();

        return view('users.index', [
            'users' => $users,
            'title' => 'Listado de usuarios',
            'roles' => trans('users.filters.roles'),
            'skills' => Skill::orderBy('name')->get(),
            'states' => trans('users.filters.states'),
            'checkedSkills' => collect(request('skills')),
        ]);
    }

    public function trashed()
    {
        $users = User::onlyTrashed()->paginate();

        $title = 'Listado de usuarios en papalera';

        return view('users.index', compact('title', 'users'));
    }

    public function show(User $user)
    {
        return view('users.show', compact('user'));
    }

    public function create()
    {
        $professions = Profession::all();
        $skills = Skill::orderBy('name', 'ASC')->get();
        $roles = trans('users.roles');
        $user = new User;

        return view('users.create', compact('professions', 'skills', 'roles', 'user'));
    }

    public function store(CreateUserRequest $request)
    {
        $request->createUser();

        return redirect()->route('users.index');
    }

    public function edit(User $user)
    {
        $professions = Profession::all();
        $skills = Skill::orderBy('name', 'ASC')->get();
        $roles = trans('users.roles');

        return view('users.edit', compact('professions', 'skills', 'roles', 'user'));
    }

    public function update(UpdateUserRequest $request, User $user)
    {
        $request->updateUser($user);

        return redirect()->route('users.show', ['user' => $user]);
    }

    public function trash(User $user)
    {
        $user->delete();
        $user->profile()->delete();

        return redirect()->route('users.index');
    }

    public function destroy($id)
    {
        $user = User::onlyTrashed()->where('id', $id)->firstOrFail();

        $user->forceDelete();

        return redirect()->route('users.trashed');
    }

    public function restore($id)
    {
        $user = User::onlyTrashed()->where('id', $id)->firstOrFail();

        $user->restore();
        
        return redirect()->route('users.index');
    }
}
