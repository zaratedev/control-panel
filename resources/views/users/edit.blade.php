@extends('layout')

@section('title', "Crear usuario")

@section('content')

    @component('shared._card')
        @slot('header')
        Editar usuario
        @endslot

        @slot('content')
            @include('shared._errors')

            <form method="POST" action="{{ url("usuarios/{$user->id}") }}">
                {{ method_field('PUT') }}
                @include('users._fields')
                <div class="form-group mt-4">
                    <button type="submit" class="btn btn-primary">Actualizar usuario</button>
                    <a href="{{ route('users.index') }}" class="btn btn-link">Regresar al listado de usuarios</a>
                </div>
            </form>
        @endslot
    @endcomponent
@endsection

@section('js')
<script>
    $("#profession_id").change(function () {
        const profession = $("#profession_id").val();
        document.getElementById('another_profession_id').value = '';
        if (profession) {
            $("#another_profession").hide();
        } else {
            $("#another_profession").show();
        }
    });
</script>
@endsection