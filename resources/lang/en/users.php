<?php

return [
    'roles' => ['admin' => 'Admin', 'user' => 'User'],
    'filters' => [
        'roles' => [
            'all' => 'Rol',
            'admins' => 'Administradores',
            'users' => 'Usuarios',
        ],
        'states' => [
            'all' => 'Todos',
            'active' => 'Solo activos',
            'inactive' => 'Solo inactivos',
        ]
    ]
];
